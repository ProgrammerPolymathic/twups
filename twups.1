.\" Manpage for twups (VERSIONPLACEHOLDER)
.\"
.\" Copyright (c) 2020 (GPL-3.0 or later)
.\"     ProgrammerPolymathic (https://codeberg.org/ProgrammerPolymathic)
.\" A copy of the licence should have been distributed with the program; it may also be found in the source repository at
.\"     https://codeberg.org/ProgrammerPolymathic/twups
.\"
.TH TWUPS 1 "DATEPLACEHOLDER" "twups VERSIONPLACEHOLDER" "User Manuals"

.SH NAME
.B twups
\- Tumbleweed Update Scrutinizer

.SH SYNOPSIS
.B twups
.RI [ OPTION ...]

.SH DESCRIPTION
Fetches and presents information from the openSUSE Tumbleweed Review hosted by Boombatower, optionally prompting to update based on stability ratings.  Fetching is handled by
.BR curl
(directed at https://review.tumbleweed.boombatower.com/data/score.yaml), snapshot selection is handled by
.BR tumbleweed-cli ,
and installation of updates is performed by
.BR zypper ". "
The latter two functionalities require
.B sudo
privileges, which will be prompted for when needed.
.PP
To select a snapshot (and by extension install it),
.B tumbleweed-cli
must first be initialized with
.RI "'" "tumbleweed init" "'"
(this of course implies that
.B tumbleweed-cli
must also be installed).
.PP
When run,
.B twups
will print a brief description of each snapshot in the Boombatower database matching the criteria given by the user, by default the most recent snapshots (see the following section for options and defaults).  These are, in order, the version number (i.e. date) of the snapshot, Boombatower's current stability rating for the snapshot, and the current stability score.  A score greater than 90 is deemed "stable," a score between 71 and 90 inclusive is deemed "moderate," and any lower score is deemed "unstable."  Snapshots less than a week old, whose scores are subject to change, are considered to be "pending."  Snapshots newer than that currently installed, the currently installed snapshot, and snapshots older than that currently installed will be labeled as such after the version number of each result.
.PP
If any of the returned snapshots meet the user-specified criteria to be selected for an update, the most recent candidate from the list will be presented, and the user asked if the update should be performed.  After an affirmative response,
.B tumbleweed-cli
will be called to switch to the desired snapshot (requiring
.B sudo
privileges) and
.B zypper
will be run interactively, allowing for a final review of packages to be modified before installation.

.SH OPTIONS
.\" Can't fit all formatting on one line to use with .TP, so indentations are handled manually
.B -c
.IR COUNT ,
.BI --count= COUNT
.RS
Specify the number of snapshots to be displayed.  When
.B --search
is not in use, this equates to displaying the most recent
.I COUNT
snapshots.  When
.B --search
has been specified, this limits the number of results.  Default is 5 when
.B --search
is not specified and unlimited otherwise.
.RE
.PP
.BI --color= WHEN
.RS
Determine whether to include colour escapes in output.  Allowed values are
.BR never , " always" ", and " auto ".  " "auto"
will enable colours if the output is a terminal and disable them otherwise; this is the default setting.
.RE
.PP
.BR -h ", " --help
.RS
Display a brief explanation of
.B twups
and its parameters, then exit.
.RE
.PP
.BR -n ", " --no-upgrade
.RS
Exit immediately after retrieving snapshots, without prompting to update.
.RE
.PP
.BR -r ", " --rollback
.RS
Permit selection of snapshots older than that currently installed.  Note that this will still only select snaphots rated "stable" unless
.B --unstable-upgrades
is also specified.
.RE
.PP
.B -s
.IR QUERY ", "
.BI --search= QUERY
.RS
Rather than displaying the most recent snapshots, search the database for snapshots with a date in either yyyymmdd or yyyy-mm-dd format matching
.IR QUERY ,
which may be a regular expression.  Results will be printed in reverse chronological order.  This is the ideal method of selecting a specific snapshot if the most recent (or most recent stable) is not desired.
.PP
By default, this will search the entire database, even if a complete date is entered (which could yield at most one result).  Combining this option with
.B --count
will not only limit the otherwise unbounded number of results, but also cease the search as soon as the specified number of results have been found, saving some time.
.RE
.PP
.BR -u ", " --unstable-upgrades
.RS
Accept snapshots not rated "stable" as candidates for updates, i.e. prompt to update to the first snapshot listed (unless that snapshot is older than the current version and
.B --rollback
is not specified).
.RE
.PP
.BR -v ", " --version
.RS
Print version and exit.
.RE

.SH EXIT STATUS
Exit codes are roughly based on FreeBSD's sysexit specification, for want of any other standard to follow.
.IP 64
Input not recognized (e.g. nonexistent parameter).
.IP 65
Invalid argument (e.g. a requested result count of "foo" rather than an integer).
.IP 69
Attemping to perform an update when
.B tumbleweed-cli
has not been initialized.
.IP 70
.B tumbleweed-cli
failed to select the requested snapshot.
.IP 75
Failure to acquire information from Boombatower (e.g. network issues, site outage).

.SH NOTES
Users should be aware that stability scores (and by extension ratings) change over time, usually dropping.  For this reason, snapshots less than a week old are labelled "pending" to reflect their volatile state, after which there is unlikely to be much further change.  Those wishing to avoid unstable snapshots should wait for this first week to pass before updating to a new release (as is the default behaviour of this program).
.PP
The currently installed snapshot can appear in output, but cannot be selected for updating, as this would under normal circumstances have no effect.  If need be, "switching" to the snapshot already marked as installed may be accomplished via the
.B --force
option available in
.BR tumbleweed-cli .

.SH BUGS
Boombatower supports bug counts for each snapshot via Bugzilla, but does not have Bugzilla access at the time of writing.  If this situation changes, bug counts may be implemented, but at the expense of pulling them from an additional file to be downloaded (as bugs are stored separately on Boombatower).
.PP
Further issues may be reported at https://codeberg.org/ProgrammerPolymathic/twups.

.SH SEE ALSO
.BR "tumbleweed --help" , " zypper" (8)
