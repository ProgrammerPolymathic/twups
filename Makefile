# Makefile for twups (0.2.3)
#
# Copyright (c) 2020 (GPL-3.0 or later)
#     ProgrammerPolymathic (https://codeberg.org/ProgrammerPolymathic)
# A copy of the licence should have been distributed with the program; it may also be found in the source repository at
#     https://codeberg.org/ProgrammerPolymathic/twups

ifeq ($(PREFIX),)
	PREFIX = /usr
endif
ifeq ($(SYSCONFIG),)
	SYSCONFIG = /etc
endif

VERSION=0.2.3
DATE=2020-11-14

all:
	@echo "No binaries to compile; install via 'make install'"

install: twups twups.1 twups-completion.bash
	@echo "Marking version..."
	sed -i 's/VERSIONPLACEHOLDER/$(VERSION)/' twups twups.1 twups-completion.bash
	sed -i 's/DATEPLACEHOLDER/$(DATE)/'       twups.1
	@echo ""
	install	-D twups                 "$(DESTDIR)$(PREFIX)/bin/twups"
	install -D twups-completion.bash "$(DESTDIR)$(SYSCONFIG)/bash_completion.d/twups-completion.bash"
	install -D twups.1               "$(DESTDIR)$(PREFIX)/share/man/man1/twups.1"
	@echo ""
	@echo "Done."

uninstall:
	@echo "Uninstalling..."
	rm "$(DESTDIR)$(PREFIX)/bin/twups"
	rm "$(DESTDIR)$(SYSCONFIG)/bash_completion.d/twups-completion.bash"
	rm "$(DESTDIR)$(PREFIX)/share/man/man1/twups.1"
	@echo "Done."
