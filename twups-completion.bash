#!/usr/bin/env bash

# Bash completion for twups (VERSIONPLACEHOLDER)
#
# Copyright (c) 2020 (GPL-3.0 or later)
#     ProgrammerPolymathic (https://codeberg.org/ProgrammerPolymathic)
# A copy of the licence should have been distributed with the program; it may also be found in the source repository at
#     https://codeberg.org/ProgrammerPolymathic/twups

complete -W "-c --count --color -h --help -n --no-upgrade -r --rollback -s --search -u --unstable-upgrades -v --version" twups
