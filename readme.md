# twups

Tumbleweed Update Scrutinizer

Fetches snapshot scores from [Boombatower](https://review.tumbleweed.boombatower.com/) and can prompt for update based on stability ratings.  Searching the snapshot database is also allowed with regex support.

## Installation

Ensure that the following prerequisites are installed (may be installed via `zypper` if not):

- curl
- tumbleweed-cli (for update functionality)

Clone the repository and run `make install` (no binaries to compile):

```
git clone https://codeberg.org/ProgrammerPolymathic/twups.git
cd twups
sudo make install
```

One may additionally uninstall `twups` via `make uninstall`.

## Usage

Some examples are shown below.  More information can be found by running `twups --help` or `man twups` after installation.

Show the 5 most recent snapshots, and prompt to update if any are newer than that currently installed and rated "stable":

```
twups
```

Show the most recent snapshot and prompt to update even if not rated "stable" (unless it's already installed):

```
twups --count 1 --unstable-upgrades
```

Show all snapshots released in January 2020, without coloured output, and don't prompt to update:

```
twups --search 2020-01-.. --color=never --no-upgrade
```

Show the snapshot released on 2020-01-03 and prompt to downgrade from a more recent snapshot, regardless of stability rating:

```
twups --search 20200103 --rollback --unstable-upgrades
```
